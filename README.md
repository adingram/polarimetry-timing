# README #

### What is this repository for? ###

This repository contains some source code to demonstrate the X-ray polatimetry-timing method of Ingram & Maccarone (2017).
Currently, the repository includes only the code used to generate Figs. 1 and 2 in Ingram & Maccarone (2017).
In future we will build upon this, and hopefully we will eventually have a full pipeline for conducting a polarimetry-timing analysis with real IXPE data!

### How do I get set up? ###

phaseresolve.f is a fortran code. The parameters of the simulation can be changed by opening the source code in a text editor.
The main parameters are listed at the top of the code (line 43 onwards):     
      ext   = .true.   !if true, read in file to get I, p and psi modulations     
...this determins whether the code is going to read in the modulations in I, p0 and psi0 from an external file (.true.), or if it is going to caluclate them itself (.false.)    
      mu    = 0.3      !Modulation factor      
...this is the modulation factor, \mu in the paper      
      tex   = 200.0e3  !Exposure time (s)      
...the exposure time, T in the paper      
      fwhm  = 0.2      !full width at half maximum (Hz)      
...\Delta in the paper      
      Ref   = 100.0    !Reference band count rate (counts/second)      
...<r> in the paper      
      rate0 = 100.0    !count rate      
...<s> in the paper      
      bbn   = 0.03     !Fractional rms of the BBN integrated over Delta      
...rms_n in the paper (see Appendix A)      

If ext=.true., the code then reads in a file called 'filenm', which is defined on line 55. This repository contains two such files:      
'Stokes4_i30_F180_b10.qdp' and 'Stokes4_i70_F110_b10.qdp',      
which are for the 'low' and 'high' inclination models respectively (as they are referred to in the paper).
Therefore, Fig 1 from Ingram & Maccarone (2017) can be recreated using Stokes4_i70_F110_b10.qdp, and Fig 2 by using Stokes4_i30_F180_b10.qdp.
For information on how the predicted modulations in I, p0 and psi0 are calculated, see Ingram et al (2015), ApJ, 807, article id. 53.

If ext=.false., the code does not read in an external file it instead creates the I, p0 and psi0 modulations from a phenomenological model with an array of parameters:      
param(1:15)      
These 15 parameters are defined on lines 56-70 of the code, and the convention is that A1r, A2r are the amplitude of the 1st and 2nd harmonics of the oscillation in total
polarimeter count rate (s(\omega) in the paper) and P1r, P2r are the phase of the 1st and 2nd harmonics of the oscillation in total polarimeter count rate. There are then
similar parameters for the p0 oscillation (p0=<p0>,A1p,P1p,A2p,P2p) and the psi0 oscillation (psi0=<psi0>,A1psi,P1psi,A2psi,P2psi).

### Running the code ###

The code can be compiled and run with (hopefully) and fortran compiler with no exotic options:      
gfortran phaseresolve.f      
./a.out      

### Code output ###

This should create two ascii files: amp.dat and lag.dat. These can simply be viewed using qdp, e.g.:      
qdp amp.dat      
The black data points are the fake data (columns 3 and 4 in the ascii file), the red line is the model that the fake data is generated from (column 5 in the ascii file),
the green line is the null hypothesis (column 6 in the ascii file), and the blue line is the best-fitting cosine model (column 7 in the ascii file). These amp.dat and
lag.dat files should make plots identical to Fig. 1 and 2 in Ingram & Maccarone (2017), except the files contain the extra line (the blue line), which is the best-fitting
cosine model (see Equation 5 in the paper).      

An example of the The terminal output is shown below:      
 <p0>=   3.50589561     %      
 <psi0>=  -18.9730453     degrees      
 rmsq(1)=   4.54383945     %      
 A1psi (degrees) =   14.2067471      
 P1psi (cycles) =  0.316358089
 ------------------------------------
 input model: ampX2/dof=   41.1964684     /          50      
 input model: lagX2/dof=   48.7719803     /          50      
 input model: X2/dof=   89.9684448     /         100
 ------------------------------------
 weighted mean lag (cycles)=   9.32508206E-04      
 weighted mean amp (%)=   5.42975044      
 null-hyp: ampX2/dof=   62.4309921     /          49      
 null-hyp: lagX2/dof=   95.7632141     /          49      
 null-hyp: X2/dof=   158.194214     /          98
 ------------------------------------
 cos model: ampX2=   39.9879875     dof=          47      
 cos model: lagX2=   45.8244591     dof=          47      
 cos model: X2=   85.8124466     dof=          94      
 ------------------------------------
 F=   19.8219681      
 p-value=   7.36509638E-12
 ------------------------------------
 To create xspec compatible files,      
 run script: flxscript.xcm
 ------------------------------------
 ascii files with 1st harmonic amplitude and lags:      
 amp.dat and lag.dat      

The first set of outputs shows some of the parameters of the input model.
The second set shows the \chi^2 obtained by comparing the input model with the fake data. Clearly, this should indicate a good fit!
The third set gives all information about the null-hypothesis model
The fourth set gives all information about the best-fitting cosine model (Equation 5 in Ingram & Maccarone 2017)
The fifth set gives the result of the F-test: i.e. by how much is the cosine model preferred to the null-hypothesis model?
Therefore the p-value is the probability that the null-hypothesis is as good as the best-fitting cosine model.

The fifth set of outputs gives the opportunity to convert the amplitude and lag fake data into xspec compatible files. The commands:      
chmod 755 flxscript.xcm      
./flxscript.xcm      
will create the files      
amp1.pha amp2.pha lag1.pha lag2.pha rate.pha      
amp1.rsp amp2.rsp lag1.rsp lag2.rsp rate.rsp      
These can be loaded into xspec in the usual way: e.g. data 1:1 amp1.pha
and the user can fit models to the fake data.
In future, we will also make public the xspec model we used in section 3.3 to measure an oscillation in polarization angle.

### Advanced options ###

The number of psi bins is set by the parameter nmax, and the number of QPO phase bins is set by the parameter nphase.
These are both set on line 9.      
### *NB* nphase must be an integer power of 2 because the code uses FFTs.

The seed of the random number generator is set in the subroutine polsynth (line 771 to 791). This can be changed by
adjusting the value of idum in the data statement on line 778.

### Who do I talk to? ###

Any questions, contact me at a.r.ingram@uva.nl