      program phaseresolve
c Code to calculate QPO FT as a function of polarization
c degree. Will then simulate just like my PhaseSim
c codes.
c
c
      implicit none
      integer nmax,j,k,nphase
      parameter (nmax=50,nphase=32)
      real pi,dpsi,psi,f,fpsi,rate(nphase),mu
      real pdeg(nphase),pang(nphase),dphi,phi
      real rate0,p0,param(15)
      real psi0,A1psi,P1psi,amp1(nmax),lag1(nmax)
      real amp2(nmax),lag2(nmax),mean
      real ReM(nmax,0:nphase/2),ImM(nmax,0:nphase/2),mt(nmax,nphase)
      complex Q(nmax,2),Qp(nmax,2)
      real S(nmax),tex,dQ(nmax,2),dS(nmax),fwhm,Ref,Sp(nmax)
      real amp1p(nmax),amp2p(nmax),damp1(nmax),damp2(nmax),lag1p(nmax)
      real lag2p(nmax),dlag1(nmax),dlag2(nmax),arg,d
      logical ext
      character (len=200) filenm
      real ReR(0:nphase/2),ImR(0:nphase/2)
      real lagmean(nmax),chisq
      real ampmean(nmax),ampX2
      real phiR(nphase/2),meanamp,lagX2
      real RePSI(0:nphase/2),ImPSI(0:nphase/2),psilo,psihi,bbn
      real rmsq(0:nphase/2)
      character (len=200) com
      integer mmax
      parameter (mmax=20)
      real modelmeanamp,modelmaxamp,amppsimax
      real modelmeanlag,modelmaxlag,lagpsimax,yamp,ylag
      real aamp(mmax),alag(mmax),x(nmax),dyda(mmax),dalag(mmax)
      external cosfunc
      real covar(mmax,mmax),alpha(mmax,mmax),alamda,daamp(mmax)
      integer ia(mmax),maxit
      parameter (maxit=100)
      real X2,nu2,X1,nu1,p,myftest,Fstat,X2calc
      pi    = acos(-1.0)
      dpsi  = 2 * pi / float(nmax)
      dphi  = 1. / float(nphase)
      
c Set parameters
      ext   = .true.   !if true, read in file to get I, p and psi modulations
      mu    = 0.3      !Modulation factor
      tex   = 200.0e3  !Exposure time (s)
      fwhm  = 0.2      !full width at half maximum (Hz)
      Ref   = 100.0    !Reference band count rate (counts/second)
      rate0 = 100.0    !count rate
      bbn   = 0.03     !Fractional rms of the BBN integrated over Delta
c nmax and nphase are set as parameters on line 9

c Set I, p & psi modulations:
c Either by reading in a file (ext=.true.), or by setting parameters (ext=.false.)
      filenm = 'Stokes4_i30_F180_b10.qdp' !'Stokes4_i70_F110_b10.qdp' 
      param(1)  = rate0
      param(2)  = 0.1 * sqrt(2.)*rate0  !A1r   count rate
      param(3)  = 0.0                   !P1r   cycles
      param(4)  = 0.08 * sqrt(2.)*rate0 !A2r   count rate
      param(5)  = 0.2                   !P2r   cycles
      param(6)  = 0.1                   !p0    fraction
      param(7)  = 0.01                  !A1p   fraction
      param(8)  = 0.0                   !P1p   cycles
      param(9)  = 0.0                   !A2p   fraction
      param(10) = 0.5                   !P2p   cycles
      param(11) = -6.5                  !psi0  degrees
      param(12) = 3.0                   !A1psi degrees
      param(13) = 0.7                   !P1psi cycles
      param(14) = 0.2                   !A2psi degrees
      param(15) = 0.0                   !P2psi cycles

c Set up files to write to
      open(94,file='amp.dat')
      write(94,*)"skip on"      !amplitude: data and model
      write(94,*)"read serr 1 2"
      write(94,*)"err off 2,4"
      write(94,*)"la x \gq (degrees)"
      write(94,*)"la y Fractional RMS (%)"
      write(94,*)"lw 5"
      write(94,*)"la file"
      write(94,*)"tim off"
      write(94,*)"ma 17 on 1"
      write(94,*)"ma size 2 on 1"
      open(93,file='lag.dat')
      write(93,*)"skip on"      !lags: data and model
      write(93,*)"read serr 1 2"
      write(93,*)"err off 2,4"
      write(93,*)"la x \gq (degrees)"
      write(93,*)"la y Phase (cycles)"
      write(93,*)"lw 5"
      write(93,*)"la file"
      write(93,*)"tim off"
      write(93,*)"ma 17 on 1"
      write(93,*)"ma size 2 on 1"
      
c Set modulations (depends on ext parameter)
      call mkmods(nphase,filenm,20,ext,param,rate,pdeg,pang,p0,psi0)
      write(*,*)"<p0>=",p0*100,"%"
      write(*,*)"<psi0>=",psi0*180./pi,"degrees"
      
c Fourier transform rate
      call doFFT(dphi,nphase,rate,ReR,ImR)
      do k = 1,nphase/2
        phiR(k) = atan2( ImR(k) , ReR(k) )
      end do
      rmsq = sqrt( ReR**2 + ImR**2 ) / rate0
      write(*,*)"rmsq(1)=",rmsq(1)*100,"%"
      
c Fourier transform polarisation angle
      call doFFT(dphi,nphase,pang,RePSI,ImPSI)
      A1psi = sqrt( 2 * (RePSI(1)**2+ImPSI(1)**2) )
      P1psi = atan2( ImPSI(1) , RePSI(1) ) - phiR(1)
      write(*,*)"A1psi (degrees) =",A1psi*180./pi
      write(*,*)"P1psi (cycles) =",P1psi/(2.*pi)
      
c Set count rate in each psi bin for each QPO phase: mt(j=psi bin,k=QPO phase)
      call mklcs(nphase,nmax,mu,rate,pdeg,pang,mt)

c Calculate QPO FT and amp and lags vs psi
      call mklagamp(nphase,nmax,mt,phiR,rmsq,bbn,
     &              Q,S,amp1,amp2,lag1,lag2,x)
      
c Calculate best guess for cosine fits from input model
      call guess(nmax,amp1,lag1,x,aamp,alag)

c Calculate errors
      call polerrors(nmax,nphase,Q,S,Ref,tex,fwhm,dQ,dS)

c Generate fake data
      call polsynth(nmax,Q,dQ,S,tex,Qp,Sp)

c Convert to amplitude and lags
      call amplagdata(nmax,Qp,dQ,S,amp1p,damp1,amp2p,damp2,
     &                      lag1p,dlag1,lag2p,dlag2)

c Calculated weighted mean for amp and lag
      call weightmean(nmax,amp1p,damp1,mean)
      ampmean = mean
      call weightmean(nmax,lag1p,dlag1,mean)
      lagmean = mean

c Calculate chisquared for input model
      ampX2 = X2calc(nmax,amp1p,damp1,amp1)
      lagX2 = X2calc(nmax,lag1p,dlag1,lag1)
      X2    = ampX2 + lagX2
      write(*,*)"------------------------------------"
      write(*,*)"input model: ampX2/dof=",ampX2,"/",nmax
      write(*,*)"input model: lagX2/dof=",lagX2,"/",nmax
      write(*,*)"input model: X2/dof=",X2,"/",2*nmax
      
c Calculate chisquared for null-hypothesis model
      ampX2 = X2calc(nmax,amp1p,damp1,ampmean)
      lagX2 = X2calc(nmax,lag1p,dlag1,lagmean)
      chisq = ampX2 + lagX2
      X1 = chisq
      write(*,*)"------------------------------------"
      write(*,*)"weighted mean lag (cycles)=",lagmean(1)
      write(*,*)"weighted mean amp (%)=",ampmean(1)
      write(*,*)"null-hyp: ampX2/dof=",ampX2,"/",nmax-1
      write(*,*)"null-hyp: lagX2/dof=",lagX2,"/",nmax-1
      write(*,*)"null-hyp: X2/dof=",chisq,"/",2*nmax-2
      write(*,*)"------------------------------------"
      
c Find best fitting cosine model
      ia = 1
      call dofit(x,amp1p,damp1,nmax,aamp,ia,3,cosfunc,ampX2,daamp)
      call dofit(x,lag1p,dlag1,nmax,alag,ia,3,cosfunc,lagX2,dalag)
      X2 = ampX2 + lagX2
      write(*,*)"cos model: ampX2=",ampX2,"dof=",nmax-3
      write(*,*)"cos model: lagX2=",lagX2,"dof=",nmax-3
      write(*,*)"cos model: X2=",X2,"dof=",2*nmax-6
      write(*,*)"------------------------------------"

c Do F-test
      nu2   = real( 2 * nmax - 6 )  !unrestricted model
      nu1   = real( 2 * nmax - 2 )  !restricted model (null-hypothesis)
      Fstat = myftest(X2,nu2,X1,nu1,p)
      write(*,*)"F=",Fstat
      write(*,*)"p-value=",p
      write(*,*)"------------------------------------"

c Write out amp and lag
      do j = 1,nmax
        !Calculate theoretical mean from model
        meanamp = sqrt( rmsq(1)**2 + bbn**2 ) * 100.0
        !Best guess cosine function
        call cosfunc(x(j),aamp,yamp,dyda,3)
        call cosfunc(x(j),alag,ylag,dyda,3)
        !Write out
        write(94,*)x(j)*180./pi,180./float(nmax),
     &    amp1p(j),damp1(j),amp1(j),meanamp,yamp
        write(93,*)x(j)*180./pi,180./float(nmax),
     &    lag1p(j),dlag1(j),lag1(j),0.0,ylag
      end do
      
c Write in flx2xsp format
c Need to write:
c Count rate vs psi
c Amp*dpsi vs psi
c Lag*dpsi vs psi
      do j = 1,nmax
        psilo = ( j - 1 ) * 360.0 / float(nmax)
        psihi =   j * 360.0 / float(nmax)
        dpsi  = 360.0 / float(nmax)
        write(80,*)psilo,psihi,dpsi*lag1p(j),dpsi*dlag1(j)
        write(79,*)psilo,psihi,dpsi*amp1p(j),dpsi*damp1(j)
        write(78,*)psilo,psihi,Sp(j),sqrt(S(j)/tex)
        write(77,*)psilo,psihi,dpsi*lag2p(j),dpsi*dlag2(j)
        write(76,*)psilo,psihi,dpsi*amp2p(j),dpsi*damp2(j)
      end do

      open(21,file='flxscript.xcm')
      com = 'flx2xsp infile=fort.80 phafil=lag1.pha'
      com = trim(com) // ' rspfil=lag1.rsp clobber=yes'
      write(21,*)trim(com)
      com = 'flx2xsp infile=fort.79 phafil=amp1.pha'
      com = trim(com) // ' rspfil=amp1.rsp clobber=yes'
      write(21,*)trim(com)
      com = 'flx2xsp infile=fort.78 phafil=rate.pha'
      com = trim(com) // ' rspfil=rate.rsp clobber=yes'
      write(21,*)trim(com)
      com = 'flx2xsp infile=fort.77 phafil=lag2.pha'
      com = trim(com) // ' rspfil=lag2.rsp clobber=yes'
      write(21,*)trim(com)
      com = 'flx2xsp infile=fort.76 phafil=amp2.pha'
      com = trim(com) // ' rspfil=amp2.rsp clobber=yes'

      
      write(21,*)trim(com)
      close(21)
      write(*,*)"To create xspec compatible files,"
      write(*,*)"run script: flxscript.xcm"
      close(94)
      close(93)
      write(*,*)"------------------------------------"
      write(*,*)"ascii files with 1st harmonic amplitude and lags:"
      write(*,*)"amp.dat and lag.dat"
      
      end

!-----------------------------------------------------------------------
      function X2calc(nmax,y,dy,ymod)
      implicit none
      integer nmax,j
      real y(nmax),dy(nmax),ymod(nmax),X2calc
      X2calc = 0.0
      do j = 1,nmax
        X2calc = X2calc + ( y(j) - ymod(j) )**2 / dy(j)**2
      end do
      return
      end
!-----------------------------------------------------------------------
      
!-----------------------------------------------------------------------
      subroutine weightmean(nmax,y,dy,mean)
c in:  nmax,amp1p,damp1
c out: ampmean
      implicit none
      integer nmax,j
      real y(nmax),dy(nmax),mean,num,den
      num = 0.0
      den = 0.0
      do j = 1,nmax
        num = num + y(j) / dy(j)**2
        den = den + 1.0 / dy(j)**2
      end do
      mean = num / den
      return
      end
!-----------------------------------------------------------------------
      
!-----------------------------------------------------------------------
      subroutine amplagdata(nmax,Qp,dQ,S,amp1p,damp1,amp2p,damp2,
     &                      lag1p,dlag1,lag2p,dlag2)
c in: nmax,Qp,dQ,S
c out: amp1p,damp1,amp2p,damp2,lag1p,dlag1,lag2p,dlag2
c Amplitude in % and lag is in cycles
      implicit none
      integer nmax,j
      real S(nmax),pi,dQ(nmax,2),arg
      real amp1p(nmax),damp1(nmax),amp2p(nmax),damp2(nmax)
      real lag1p(nmax),dlag1(nmax),lag2p(nmax),dlag2(nmax)
      complex Qp(nmax,2)
      pi = acos(-1.0)
      do j = 1,nmax
        amp1p(j) = abs( Qp(j,1) ) / S(j) * 100
        damp1(j) = dQ(j,1) / S(j) * 100
        amp2p(j) = abs( Qp(j,2) ) / S(j) * 100
        damp2(j) = dQ(j,2) / S(j) * 100
        lag1p(j) = arg( Qp(j,1) ) / (2.*pi)
        dlag1(j) = dQ(j,1) / abs( Qp(j,1) ) / (2.*pi)
        lag2p(j) = arg( Qp(j,2) ) / (2.*pi)
        dlag2(j) = dQ(j,2) / abs( Qp(j,2) ) / (2.*pi)
      end do
      return
      end
!-----------------------------------------------------------------------
      
!-----------------------------------------------------------------------
      subroutine guess(nmax,amp1,lag1,x,aamp,alag)
c in: nmax,amp1,lag1,x
c out: aamp,alag
      implicit none
      integer nmax,j
      real amp1(nmax),lag1(nmax),x(nmax),aamp(3),alag(3)
      real modelmeanamp,modelmaxamp,amppsimax
      real modelmeanlag,modelmaxlag,lagpsimax
      modelmeanamp = 0.0
      modelmaxamp  = 0.0
      amppsimax    = 0.0
      modelmeanlag = 0.0
      modelmaxlag  = 0.0
      lagpsimax    = 0.0
      do j = 1,nmax
        modelmeanamp = modelmeanamp + amp1(j)
        modelmeanlag = modelmeanlag + lag1(j)
        if( j .lt. nmax/2+1 )then
          if( amp1(j) .gt. modelmaxamp )then
            modelmaxamp = amp1(j)
            amppsimax   = x(j)
          end if
          if( lag1(j) .gt. modelmaxlag )then
            modelmaxlag = lag1(j)
            lagpsimax   = x(j)
          end if
        end if
      end do
      modelmeanamp = modelmeanamp / float(nmax)
      modelmeanlag = modelmeanlag / float(nmax)
c Now can guess parameters for cosine fit
      aamp(1) = modelmeanamp
      aamp(2) = modelmaxamp - modelmeanamp
      aamp(3) = amppsimax
      alag(1) = modelmeanlag
      alag(2) = modelmaxlag - modelmeanlag
      alag(3) = lagpsimax
      return
      end
!-----------------------------------------------------------------------
      

!-----------------------------------------------------------------------
      subroutine mklagamp(nphase,nmax,mt,phiR,rmsq,bbn,
     &                    Q,S,amp1,amp2,lag1,lag2,x)
c Routine to calculate QPO FT and amplitude and lags as a function
c of modulation angle, psi
      implicit none
      integer nphase,nmax
      real mt(nmax,nphase),S(nmax),amp1(nmax),amp2(nmax),x(nmax)
      real lag1(nmax),lag2(nmax),phiR(nphase/2),rmsq(0:nphase/2),bbn
      complex Q(nmax,2)
      integer j,k
      real dphi,ReM(nmax,0:nphase/2),ImM(nmax,0:nphase/2),pi,dpsi,arg
      pi   = acos(-1.0)
      dphi = 1. / float(nphase)
      dpsi = 2 * pi / float(nmax)
      do j = 1,nmax
        !Calculate FFT
        call doFFT(dphi,nphase,mt(j,:),ReM(j,:),ImM(j,:))
        S(j) = ReM(j,0)
        do k = 1,2
          !First make QPO bit
          Q(j,k) = complex( ReM(j,k) , ImM(j,k) )
          Q(j,k) = Q(j,k) * complex( cos(phiR(k)) , -sin(phiR(k)) )
          !Then add on BBN
          Q(j,k) = Q(j,k) * rmsq(k) + S(j) * bbn**2
          Q(j,k) = Q(j,k) / sqrt( rmsq(k)**2 + bbn**2 )
        end do
        !Calculate amplitude and lags
        amp1(j) = abs( Q(j,1) ) / S(j) * 100
        amp2(j) = abs( Q(j,2) ) / S(j) * 100
        lag1(j) = arg( Q(j,1) ) / (2.*pi)
        lag2(j) = arg( Q(j,2) ) / (2.*pi)
        x(j)    = ( j - 0.5 ) * dpsi
      end do          
      return
      end
!-----------------------------------------------------------------------
      
!-----------------------------------------------------------------------
      subroutine mklcs(nphase,nmax,mu,rate,pdeg,pang,mt)
c Routine to make nmax light curves with nphase steps
c Each light curve is for a different psi bin
      implicit none
      integer nphase,nmax,j,k
      real mu,rate(nphase),pdeg(nphase),pang(nphase)
      real mt(nmax,nphase),psi,dpsi,pi,f,fpsi
      pi    = acos(-1.0)
      dpsi  = 2 * pi / float(nmax)
      do k = 1,nphase
        do j = 1,nmax
          psi = ( j - 0.5 ) * dpsi
          !Calculate modulation function
          f = fpsi(psi,mu,pdeg(k),pang(k) )
          !Calculate baseline counts in the time bin
          mt(j,k) = rate(k) * f * dpsi
        end do
      end do
      return
      end
!-----------------------------------------------------------------------
      

!-----------------------------------------------------------------------
      subroutine mkmods(nphase,filenm,filenum,ext,param,rate,
     &                  pdeg,pang,p0,psi0)
c in:  nphase,filenm,filenum,ext,param
c out: rate,pdeg,pang,p0,psi0
      implicit none
      integer nphase,filenum,k
      real param(*),rate(nphase),pdeg(nphase),pang(nphase),p0,psi0
      logical ext
      character (len=200) filenm
      real d,pi,rate0,A1r,P1r,A2r,P2r,A1p,P1p,A2p,P2p
      real A1psi,P1psi,A2psi,P2psi,phi,dphi
      pi    = acos(-1.0)
      dphi  = 1. / float(nphase)
      rate0 = param(1)
      if( ext )then
        !Read in modulations in rate, p and psi
        open(filenum,file=filenm)
        p0   = 0.0
        psi0 = 0.0
        do k = 1,nphase
          read(filenum,*)d,d,rate(k),d,pdeg(k),d,pang(k)
          rate(k) = rate(k) * rate0
          pdeg(k) = pdeg(k) / 100.0
          p0      = p0 + pdeg(k)
          pang(k) = pang(k) * pi / 180.0
          psi0    = psi0 + pang(k)
        end do
        p0   = p0 / float(nphase)
        psi0 = psi0 / float(nphase)
        close(filenum)
      else
        A1r    = param(2)
        P1r    = param(3)
        A2r    = param(4)
        P2r    = param(5)
        p0     = param(6)
        A1p    = param(7)
        P1p    = param(8)
        A2p    = param(9)
        P2p    = param(10)
        psi0   = param(11)
        A1psi  = param(12)
        P1psi  = param(13)
        A2psi  = param(14)
        P2psi  = param(15)
        !Calculate modulations in rate, p and psi
        do k = 1,nphase
          phi = (k-1) * dphi
          !count rate
          rate(k) = rate0 + A1r*sin( 2.*pi* (phi-P1r) )
          rate(k) = rate(k) + A2r*sin( 4.*pi* (phi-P2r) )
          !polarization degree
          pdeg(k) = p0 + A1p*sin( 2.*pi* (phi-P1p) )
          pdeg(k) = pdeg(k) + A2p*sin( 4.*pi* (phi-P2p) )
          !polarization angle
          pang(k) = psi0 + A1psi*sin( 2.*pi* (phi-P1psi) )
          pang(k) = pang(k) + A2psi*sin( 4.*pi* (phi-P2psi) )
          pang(k) = pang(k) * pi / 180.0
        end do
      end if
      return
      end
!-----------------------------------------------------------------------
      

!-----------------------------------------------------------------------
      subroutine dofit(x,y,sig,ndata,a,ia,ma,cosfunc,chisq,da)
c Routine to fit to data using mrqmin
      implicit none
      integer mmax,ndata,ma
      integer ia(ma),k,maxit
      parameter (mmax=20,maxit=100)
      real x(ndata),y(ndata),sig(ndata),a(ma),chisq,da(ma)
      external cosfunc
      real alamda,covar(mmax,mmax),alpha(mmax,mmax)
      alamda = -1.0
      call mrqmin(x,y,sig,ndata,a,ia,ma,covar,alpha,mmax,chisq,
     &     cosfunc,alamda)
      !Run the fit loop
      k = 0
      do while( alamda .lt. 1e10 .and. k .lt. maxit )
        k = k + 1
        call mrqmin(x,y,sig,ndata,a,ia,ma,covar,alpha,mmax,chisq,
     &     cosfunc,alamda)
      end do
      if( k .eq. maxit ) write(*,*)"Warning! Ran out of steps in fit!"
      !Calculate errors
      alamda = 0.0
      call mrqmin(x,y,sig,ndata,a,ia,ma,covar,alpha,mmax,chisq,
     &     cosfunc,alamda)
      do k = 1,ma
        da(k) = sqrt( abs(covar(k,k)) )
      end do
      return      
      end
!-----------------------------------------------------------------------

      
!-----------------------------------------------------------------------
      SUBROUTINE mrqmin(x,y,sig,ndata,a,ia,ma,covar,alpha,nca,chisq,
     *funcs,alamda)
      INTEGER ma,nca,ndata,ia(ma),MMAX
      REAL alamda,chisq,a(ma),alpha(nca,nca),covar(nca,nca),
     *     sig(ndata),x(ndata),y(ndata)
      external funcs
      PARAMETER (MMAX=20)
CU    USES covsrt,gaussj,mrqcof
      INTEGER j,k,l,m,mfit
      REAL ochisq,atry(MMAX),beta(MMAX),da(MMAX)
      SAVE ochisq,atry,beta,da,mfit
      if(alamda.lt.0.)then
        mfit=0
        do 11 j=1,ma
          if (ia(j).ne.0) mfit=mfit+1
11      continue
        alamda=0.001
        call mrqcof(x,y,sig,ndata,a,ia,ma,alpha,beta,nca,chisq,funcs)
        ochisq=chisq
        do 12 j=1,ma
          atry(j)=a(j)
12      continue
      endif
      j=0
      do 14 l=1,ma
        if(ia(l).ne.0) then
          j=j+1
          k=0
          do 13 m=1,ma
            if(ia(m).ne.0) then
              k=k+1
              covar(j,k)=alpha(j,k)
            endif
13        continue
          covar(j,j)=alpha(j,j)*(1.+alamda)
          da(j)=beta(j)
        endif
14    continue
      call gaussj(covar,mfit,nca,da,1,1)
      if(alamda.eq.0.)then
        call covsrt(covar,nca,ma,ia,mfit)
        return
      endif
      j=0
      do 15 l=1,ma
        if(ia(l).ne.0) then
          j=j+1
          atry(l)=a(l)+da(j)
        endif
15    continue
      call mrqcof(x,y,sig,ndata,atry,ia,ma,covar,da,nca,chisq,funcs)
      if(chisq.lt.ochisq)then
        alamda=0.1*alamda
        ochisq=chisq
        j=0
        do 17 l=1,ma
          if(ia(l).ne.0) then
            j=j+1
            k=0
            do 16 m=1,ma
              if(ia(m).ne.0) then
                k=k+1
                alpha(j,k)=covar(j,k)
              endif
16          continue
            beta(j)=da(j)
            a(l)=atry(l)
          endif
17      continue
      else
        alamda=10.*alamda
        chisq=ochisq
      endif
      return
      END
C  (C) Copr. 1986-92 Numerical Recipes Software .
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
      SUBROUTINE mrqcof(x,y,sig,ndata,a,ia,ma,alpha,beta,nalp,chisq,
     *funcs)
      INTEGER ma,nalp,ndata,ia(ma),MMAX
      REAL chisq,a(ma),alpha(nalp,nalp),beta(ma),sig(ndata),x(ndata),
     *y(ndata)
      EXTERNAL funcs
      PARAMETER (MMAX=20)
      INTEGER mfit,i,j,k,l,m
      REAL dy,sig2i,wt,ymod,dyda(MMAX)
      mfit=0
      do 11 j=1,ma
        if (ia(j).ne.0) mfit=mfit+1
11    continue
      do 13 j=1,mfit
        do 12 k=1,j
          alpha(j,k)=0.
12      continue
        beta(j)=0.
13    continue
      chisq=0.
      do 16 i=1,ndata
        call funcs(x(i),a,ymod,dyda,ma)
        sig2i=1./(sig(i)*sig(i))
        dy=y(i)-ymod
        j=0
        do 15 l=1,ma
          if(ia(l).ne.0) then
            j=j+1
            wt=dyda(l)*sig2i
            k=0
            do 14 m=1,l
              if(ia(m).ne.0) then
                k=k+1
                alpha(j,k)=alpha(j,k)+wt*dyda(m)
              endif
14          continue
            beta(j)=beta(j)+dy*wt
          endif
15      continue
        chisq=chisq+dy*dy*sig2i
16    continue
      do 18 j=2,mfit
        do 17 k=1,j-1
          alpha(k,j)=alpha(j,k)
17      continue
18    continue
      return
      END
C  (C) Copr. 1986-92 Numerical Recipes Software .
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
      SUBROUTINE covsrt(covar,npc,ma,ia,mfit)
      INTEGER ma,mfit,npc,ia(ma)
      REAL covar(npc,npc)
      INTEGER i,j,k
      REAL swap
      do 12 i=mfit+1,ma
        do 11 j=1,i
          covar(i,j)=0.
          covar(j,i)=0.
11      continue
12    continue
      k=mfit
      do 15 j=ma,1,-1
        if(ia(j).ne.0)then
          do 13 i=1,ma
            swap=covar(i,k)
            covar(i,k)=covar(i,j)
            covar(i,j)=swap
13        continue
          do 14 i=1,ma
            swap=covar(k,i)
            covar(k,i)=covar(j,i)
            covar(j,i)=swap
14        continue
          k=k-1
        endif
15    continue
      return
      END
C  (C) Copr. 1986-92 Numerical Recipes Software .
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
      SUBROUTINE gaussj(a,n,np,b,m,mp)
      INTEGER m,mp,n,np,NMAX
      REAL a(np,np),b(np,mp)
      PARAMETER (NMAX=50)
      INTEGER i,icol,irow,j,k,l,ll,indxc(NMAX),indxr(NMAX),ipiv(NMAX)
      REAL big,dum,pivinv
      do 11 j=1,n
        ipiv(j)=0
11    continue
      do 22 i=1,n
        big=0.
        do 13 j=1,n
          if(ipiv(j).ne.1)then
            do 12 k=1,n
              if (ipiv(k).eq.0) then
                if (abs(a(j,k)).ge.big)then
                  big=abs(a(j,k))
                  irow=j
                  icol=k
                endif
              else if (ipiv(k).gt.1) then
                write(*,*)'singular matrix in gaussj'
              endif
12          continue
          endif
13      continue
        ipiv(icol)=ipiv(icol)+1
        if (irow.ne.icol) then
          do 14 l=1,n
            dum=a(irow,l)
            a(irow,l)=a(icol,l)
            a(icol,l)=dum
14        continue
          do 15 l=1,m
            dum=b(irow,l)
            b(irow,l)=b(icol,l)
            b(icol,l)=dum
15        continue
        endif
        indxr(i)=irow
        indxc(i)=icol
        if (a(icol,icol).eq.0.) write(*,*)'singular matrix in gaussj'
        pivinv=1./a(icol,icol)
        a(icol,icol)=1.
        do 16 l=1,n
          a(icol,l)=a(icol,l)*pivinv
16      continue
        do 17 l=1,m
          b(icol,l)=b(icol,l)*pivinv
17      continue
        do 21 ll=1,n
          if(ll.ne.icol)then
            dum=a(ll,icol)
            a(ll,icol)=0.
            do 18 l=1,n
              a(ll,l)=a(ll,l)-a(icol,l)*dum
18          continue
            do 19 l=1,m
              b(ll,l)=b(ll,l)-b(icol,l)*dum
19          continue
          endif
21      continue
22    continue
      do 24 l=n,1,-1
        if(indxr(l).ne.indxc(l))then
          do 23 k=1,n
            dum=a(k,indxr(l))
            a(k,indxr(l))=a(k,indxc(l))
            a(k,indxc(l))=dum
23        continue
        endif
24    continue
      return
      END
C  (C) Copr. 1986-92 Numerical Recipes Software .
!-----------------------------------------------------------------------
      
!-----------------------------------------------------------------------
      subroutine cosfunc(x,a,ymod,dyda,ma)
      implicit none
      integer ma,mmax
      parameter (mmax=20)
      real x,a(mmax),ymod,dyda(mmax)
      ymod    = a(1) + a(2) * cos( 2 * (x - a(3) ) )
      dyda(1) = 1
      dyda(2) = cos( 2 * ( x - a(3) ) )
      dyda(3) = 2 * a(2) * sin( 2 * ( x - a(3) ) )
      return
      end
!-----------------------------------------------------------------------

      
!-----------------------------------------------------------------------
      function arg( z )
      implicit none
      complex z
      real arg
      arg = atan2( aimag(z) , real(z) )
      return
      end
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
      subroutine polsynth(numchn,Q,dQ,S,T,Qp,Sp)
      implicit none
      integer numchn,i,j,idum
      complex Q(numchn,2),Qp(numchn,2)
      real dQ(numchn,2),S(numchn),Sp(numchn),gasdev
      real ReQp,ImQp,poidev,T
c      data idum/-495848294/
c      data idum/-69534/
      data idum/-69535/
      save idum
      do i = 1,numchn
        do j = 1,2
          ReQp = real(Q(i,j))  + gasdev(idum) * dQ(i,j)
          ImQp = aimag(Q(i,j)) + gasdev(idum) * dQ(i,j)
          Qp(i,j) = complex( ReQp , ImQp )
        end do
        !Adjust the exposure time to get statistics right)
        Sp(i) = poidev( T*S(I) ,idum ) / T
      end do
      return
      end
!-----------------------------------------------------------------------



!-----------------------------------------------------------------------
      subroutine polerrors(nmax,nphase,Q,S,Ref,T,fwhm,dQ,dS)
c INPUT
c nmax       = number of psi bins
c nphase     = number of phase bins
c Q(nmax,2)  = QPO FT as a function of psi for two harmonics
c S(nmax)    = Mean count rate as a function of psi
c Ref        = Mean count rate in reference band
c T          = Exposure time
c fwhm       = full width at half maximum
c OUTPUTS:
c dQ(nmax,2) = Error on QPO FT vs psi
c dS(nmax)   = Error on mean count rate vs psi
      implicit none
      integer nmax,i,nphase,j
      complex Q(nmax,2),R(2)
      real S(nmax),Ref,T,dQ(nmax,2),dS(nmax),fwhm
      real mu,P(2),N,Pe,Ne

c Error on counts is trivially easy
      do i = 1,nmax
        dS(i)   = sqrt( S(i) / T )
      end do

c Work out Poisson noise in reference band, N
      N = 2.0 * Ref

c Work out power spectrum in reference band
      !First mean count rate in polarimeter
      mu = 0.0
      do i = 1,nmax
        mu = mu + S(i)
      end do
      !Then power spectrum of polarimater
      do j = 1,2
        R(j) = 0.0
        do i = 1,nmax
          R(j) = R(j) + Q(i,j)
        end do
        P(j) = abs( R(j) )**2 / fwhm
      end do
      !Finally, adjust power spectrum for different mean
      P = P * (Ref / mu)**2
      
c Now work out error for the rest (much harder!)
      do j = 1,2
        do i = 1,nmax
          !Power spectrum in band psi
          Pe = abs( Q(i,j) )**2 / fwhm
          !Poisson noise
          Ne = 2.0 * S(i)
          !Error on QPO FT
          dQ(i,j) = (P(j)+N) * (Pe+Ne)
          dQ(i,j) = dQ(i,j) / ( 2.0*T*P(j) )
          dQ(i,j) = sqrt( dQ(i,j) )
        end do
      end do
      return
      end
!-----------------------------------------------------------------------





!------------------------------------------------------------------------
      subroutine doFFT(dt,n,at,ReA,ImA)
      implicit none
      integer n,j
      real at(n),ReA(0:n/2),ImA(0:n/2)
      real data(2*n),dt,mu
      mu = 0.0
      do j = 1,n
        data(2*j-1) = at(j)
        data(2*j)   = 0.0
        mu = mu + at(j)
      end do
      mu = mu / float(n)
      call four1(data,n,1)
      do j = 1, n/2
        ReA(j) = data(2*j+1) * sqrt( 2. * dt / float(n) )
        ImA(j) = data(2*j+2) * sqrt( 2. * dt / float(n) )
      end do
      ReA(0) = mu
      ImA(0) = 0.0
      return
      end
!------------------------------------------------------------------------



!-----------------------------------------------------------------------
      function fpsi(psi,mu,p0,psi0)
c psi & psi0 in radians
      implicit none
      real fpsi,mu,p0,psi,psi0
      real pi
      pi   = acos(-1.0)
      fpsi = 1. + p0*mu * cos( 2.*(psi-psi0) )
      fpsi = fpsi / ( 2. * pi )
      return
      end
!-----------------------------------------------------------------------



!-----------------------------------------------------------------------
      SUBROUTINE four1(data,nn,isign)
      INTEGER isign,nn
      REAL data(2*nn)
      INTEGER i,istep,j,m,mmax,n
      REAL tempi,tempr
      DOUBLE PRECISION theta,wi,wpi,wpr,wr,wtemp
      n=2*nn
      j=1
      do 11 i=1,n,2
        if(j.gt.i)then
          tempr=data(j)
          tempi=data(j+1)
          data(j)=data(i)
          data(j+1)=data(i+1)
          data(i)=tempr
          data(i+1)=tempi
        endif
        m=n/2
1       if ((m.ge.2).and.(j.gt.m)) then
          j=j-m
          m=m/2
        goto 1
        endif
        j=j+m
11    continue
      mmax=2
2     if (n.gt.mmax) then
        istep=2*mmax
        theta=6.28318530717959d0/(isign*mmax)
        wpr=-2.d0*sin(0.5d0*theta)**2
        wpi=sin(theta)
        wr=1.d0
        wi=0.d0
        do 13 m=1,mmax,2
          do 12 i=m,n,istep
            j=i+mmax
            tempr=sngl(wr)*data(j)-sngl(wi)*data(j+1)
            tempi=sngl(wr)*data(j+1)+sngl(wi)*data(j)
            data(j)=data(i)-tempr
            data(j+1)=data(i+1)-tempi
            data(i)=data(i)+tempr
            data(i+1)=data(i+1)+tempi
12        continue
          wtemp=wr
          wr=wr*wpr-wi*wpi+wr
          wi=wi*wpr+wtemp*wpi+wi
13      continue
        mmax=istep
      goto 2
      endif
      return
      end
!-----------------------------------------------------------------------


!-----------------------------------------------------------------------
      FUNCTION gasdev(idum)
      INTEGER idum
      REAL gasdev
CU    USES ran1
      INTEGER iset
      REAL fac,gset,rsq,v1,v2,ran1
      SAVE iset,gset
      DATA iset/0/
      if (iset.eq.0) then
1       v1=2.*ran1(idum)-1.
        v2=2.*ran1(idum)-1.
        rsq=v1**2+v2**2
        if(rsq.ge.1..or.rsq.eq.0.)goto 1
        fac=sqrt(-2.*log(rsq)/rsq)
        gset=v1*fac
        gasdev=v2*fac
        iset=1
      else
        gasdev=gset
        iset=0
      endif
      return
      END
!-----------------------------------------------------------------------



!-----------------------------------------------------------------------
      function poidev(xm,idum)
      implicit none
      integer idum
      real poidev,xm,pi
      parameter (pi=3.141592654)
      real alxm,em,g,oldm,sq,t,y,gammln,ran1
      save alxm,g,oldm,sq
      data oldm /-1./
      if (xm.lt.12)then
        if(xm.ne.oldm)then
          oldm=xm
          g = exp(-xm)
        end if
        em = -1.
        t  = 1.
 2      em = em + 1.
        t = t * ran1(idum)
        if( t.gt.g) goto 2
      else
        if(xm.ne.oldm)then
          oldm=xm
          sq = sqrt(2.*xm)
          alxm = log(xm)
          g = xm*alxm-gammln(xm+1.)
        end if
 1      y = tan(pi*ran1(idum))
        em = sq*y+xm
        if(em.lt.0.) goto 1
        em = int(em)
        t = 0.9*(1.+y**2.)*exp(em*alxm-gammln(em+1.)-g)
        if(ran1(idum).gt.t) goto 1
      end if
      poidev=em
      return
      end
!-----------------------------------------------------------------------




!-----------------------------------------------------------------------
      FUNCTION gammln(xx)
      REAL gammln,xx
      INTEGER j
      DOUBLE PRECISION ser,stp,tmp,x,y,cof(6)
      SAVE cof,stp
      DATA cof,stp/76.18009172947146d0,-86.50532032941677d0,
     *24.01409824083091d0,-1.231739572450155d0,.1208650973866179d-2,
     *-.5395239384953d-5,2.5066282746310005d0/
      x=xx
      y=x
      tmp=x+5.5d0
      tmp=(x+0.5d0)*log(tmp)-tmp
      ser=1.000000000190015d0
      do 11 j=1,6
        y=y+1.d0
        ser=ser+cof(j)/y
11    continue
      gammln=tmp+log(stp*ser/x)
      return
      END
!-----------------------------------------------------------------------


!-----------------------------------------------------------------------
      FUNCTION ran1(idum)
      INTEGER idum,IA,IM,IQ,IR,NTAB,NDIV
      REAL ran1,AM,EPS,RNMX
      PARAMETER (IA=16807,IM=2147483647,AM=1./IM,IQ=127773,IR=2836,
     *NTAB=32,NDIV=1+(IM-1)/NTAB,EPS=1.2e-7,RNMX=1.-EPS)
      INTEGER j,k,iv(NTAB),iy
      SAVE iv,iy
      DATA iv /NTAB*0/, iy /0/
      if (idum.le.0.or.iy.eq.0) then
        idum=max(-idum,1)
        do 11 j=NTAB+8,1,-1
          k=idum/IQ
          idum=IA*(idum-k*IQ)-IR*k
          if (idum.lt.0) idum=idum+IM
          if (j.le.NTAB) iv(j)=idum
11      continue
        iy=iv(1)
      endif
      k=idum/IQ
      idum=IA*(idum-k*IQ)-IR*k
      if (idum.lt.0) idum=idum+IM
      j=1+iy/NDIV
      iy=iv(j)
      iv(j)=idum
      ran1=min(AM*iy,RNMX)
      return
      END
!-----------------------------------------------------------------------


!-----------------------------------------------------------------------
      subroutine mkfourchar(i,A)
c
c input the integer i and this outputs a character A which has
c a length of 4 with leading zeros if required
c
      integer i
      character (LEN=4) A
      if( i .lt. 10 )then
        write(A,'(A3,I1)')'000',i
      else if( i .lt. 100 )then
        write(A,'(A2,I2)')'00',i
      else if( i .lt. 1000 )then
        write(A,'(A1,I3)')'0',i
      else if( i .lt. 10000 )then
        write(A,'(I4)')i
      else
        write(*,*)"Integer too long in mkfourchar!"
      end if
      RETURN
      END
!-----------------------------------------------------------------------



      
!-----------------------------------------------------------------------
      function myftest(X2,nu2,X1,nu1,p)
c X2  = chisquared of unrestricted (more parameters) model
c nu2 = d.o.f. of unrestricted (more parameters) model
c X1  = chisquared of restricted (less parameters) model
c nu1 = d.o.f. of restricted (less parameters) model
c ftest = value of f-statistic
c p     = p-value (probability that X2 is only better than X1 by chance)
      implicit none
      real X2,nu2,X1,nu1,myftest,p,F,a,b,x,betai
c Calculate f-statistic
      F   = ( X1 - X2 ) / ( nu1 - nu2 )
      F   = F / ( X2 / nu2 )
      myftest = F
c Calculate p-value
      a = 0.5 * nu2
      b = 0.5 * ( nu1 - nu2 )
      x = nu2 / ( nu2 + ( nu1 - nu2 )*F  )
      p = betai(a,b,x)
      return
      end
!-----------------------------------------------------------------------





      
!-----------------------------------------------------------------------
      FUNCTION betai(a,b,x)
      real betai,a,b,x
CU    USES betacf,gammln
      real bt,betacf,gammln
      if(x.lt.0..or.x.gt.1.) write(*,*)'bad argument x in betai'
      if(x.eq.0..or.x.eq.1.)then
        bt=0.
      else
        bt=exp(gammln(a+b)-gammln(a)-gammln(b)+a*log(x)+b*log(1.-x))
      endif
      if(x.lt.(a+1.)/(a+b+2.))then
        betai=bt*betacf(a,b,x)/a
        return
      else
        betai=1.-bt*betacf(b,a,1.-x)/b
        return
      endif
      END
C     (C) Copr. 1986-92 Numerical Recipes Software v%1jw#<0(9p#3.
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
      FUNCTION betacf(a,b,x)
      INTEGER MAXIT
      real betacf,a,b,x,EPS,FPMIN
      PARAMETER (MAXIT=100,EPS=3.e-7,FPMIN=1.e-30)
      INTEGER m,m2
      real aa,c,d,del,h,qab,qam,qap
      qab=a+b
      qap=a+1.
      qam=a-1.
      c=1.
      d=1.-qab*x/qap
      if(abs(d).lt.FPMIN)d=FPMIN
      d=1./d
      h=d
      do 11 m=1,MAXIT
        m2=2*m
        aa=m*(b-m)*x/((qam+m2)*(a+m2))
        d=1.+aa*d
        if(abs(d).lt.FPMIN)d=FPMIN
        c=1.+aa/c
        if(abs(c).lt.FPMIN)c=FPMIN
        d=1./d
        h=h*d*c
        aa=-(a+m)*(qab+m)*x/((a+m2)*(qap+m2))
        d=1.+aa*d
        if(abs(d).lt.FPMIN)d=FPMIN
        c=1.+aa/c
        if(abs(c).lt.FPMIN)c=FPMIN
        d=1./d
        del=d*c
        h=h*del
        if(abs(del-1.).lt.EPS)goto 1
11    continue
      write(*,*)'a or b too big, or MAXIT too small in betacf'
1     betacf=h
      return
      END
C  (C) Copr. 1986-92 Numerical Recipes Software v%1jw#<0(9p#3.
!-----------------------------------------------------------------------







      FUNCTION artbis(func,x1,x2,xacc,par)
      INTEGER JMAX
      REAL rtbis,x1,x2,xacc,func,par(*)
      EXTERNAL func
      PARAMETER (JMAX=40)
      INTEGER j
      REAL dx,f,fmid,xmid
      fmid=func(x2,par)
      f=func(x1,par)
      if(f*fmid.ge.0.) write(*,*) 'root must be bracketed in rtbis'
      if(f.lt.0.)then
        artbis=x1
        dx=x2-x1
      else
        artbis=x2
        dx=x1-x2
      endif
      do 11 j=1,JMAX
        dx=dx*.5
        xmid=artbis+dx
        fmid=func(xmid,par)
        if(fmid.le.0.)artbis=xmid
        if(abs(dx).lt.xacc .or. fmid.eq.0.) return
11    continue
      write(*,*) 'too many bisections in rtbis'
      END
C  (C) Copr. 1986-92 Numerical Recipes Software .








      

      FUNCTION gammq(a,x)
      REAL a,gammq,x
CU    USES gcf,gser
      REAL gammcf,gamser,gln
      if(x.lt.0..or.a.le.0.)write(*,*) 'bad arguments in gammq'
      if(x.lt.a+1.)then
        call gser(gamser,a,x,gln)
        gammq=1.-gamser
      else
        call gcf(gammcf,a,x,gln)
        gammq=gammcf
      endif
      return
      END
C     (C) Copr. 1986-92 Numerical Recipes Software .

      SUBROUTINE gcf(gammcf,a,x,gln)
      INTEGER ITMAX
      REAL a,gammcf,gln,x,EPS,FPMIN
      PARAMETER (ITMAX=100,EPS=3.e-7,FPMIN=1.e-30)
CU    USES gammln
      INTEGER i
      REAL an,b,c,d,del,h,gammln
      gln=gammln(a)
      b=x+1.-a
      c=1./FPMIN
      d=1./b
      h=d
      do 11 i=1,ITMAX
        an=-i*(i-a)
        b=b+2.
        d=an*d+b
        if(abs(d).lt.FPMIN)d=FPMIN
        c=b+an/c
        if(abs(c).lt.FPMIN)c=FPMIN
        d=1./d
        del=d*c
        h=h*del
        if(abs(del-1.).lt.EPS)goto 1
11    continue
      write(*,*) 'a too large, ITMAX too small in gcf'
1     gammcf=exp(-x+a*log(x)-gln)*h
      return
      END
C     (C) Copr. 1986-92 Numerical Recipes Software .


      SUBROUTINE gser(gamser,a,x,gln)
      INTEGER ITMAX
      REAL a,gamser,gln,x,EPS
      PARAMETER (ITMAX=100,EPS=3.e-7)
CU    USES gammln
      INTEGER n
      REAL ap,del,sum,gammln
      gln=gammln(a)
      if(x.le.0.)then
        if(x.lt.0.)write(*,*) 'x < 0 in gser'
        gamser=0.
        return
      endif
      ap=a
      sum=1./a
      del=sum
      do 11 n=1,ITMAX
        ap=ap+1.
        del=del*x/ap
        sum=sum+del
        if(abs(del).lt.abs(sum)*EPS)goto 1
11    continue
      write(*,*) 'a too large, ITMAX too small in gser'
1     gamser=sum*exp(-x+a*log(x)-gln)
      return
      END
C     (C) Copr. 1986-92 Numerical Recipes Software .
      


      FUNCTION erf(x)
      REAL erf,x
CU    USES gammp
      REAL gammp
      if(x.lt.0.)then
        erf=-gammp(.5,x**2)
      else
        erf=gammp(.5,x**2)
      endif
      return
      END
C     (C) Copr. 1986-92 Numerical Recipes Software .




      FUNCTION gammp(a,x)
      REAL a,gammp,x
CU    USES gcf,gser
      REAL gammcf,gamser,gln
      if(x.lt.0..or.a.le.0.)write(*,*) 'bad arguments in gammp'
      if(x.lt.a+1.)then
        call gser(gamser,a,x,gln)
        gammp=gamser
      else
        call gcf(gammcf,a,x,gln)
        gammp=1.-gammcf
      endif
      return
      END
C  (C) Copr. 1986-92 Numerical Recipes Software .

